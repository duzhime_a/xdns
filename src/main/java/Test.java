import javax.swing.text.Style;

/**
 * @author ashang.peng@aliyun.com
 * @date 十一月 28, 2016
 */
public class Test {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person person = new Person("json","15");
        Student student = new Student(person,"teacher1");
        Student student1 = (Student) student.clone();
        student1.getPerson().setAge("16");
        student1.setTeacherName("test");
        System.out.println(student);
        System.out.println(student1);
}
}

class Person implements Cloneable{
    String name;
    String age;

    public Person(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

class Student implements Cloneable{
    private Person person;
    private String teacherName;

    public Student(Person person, String teacherName) {
        this.person = person;
        this.teacherName = teacherName;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    @Override
    public String toString() {
        return "Student{" +
                "person=" + person +
                ", teacherName='" + teacherName + '\'' +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
