package me.duzhi.test;

/**
 * Created by ashang.peng on 2016/11/23.
 */
public class LanHan {
    public static LanHan _instance = null;

    public LanHan getInstance() {
        if (_instance == null) {
            synchronized (_instance) {
                if (_instance == null) {
                    _instance = new LanHan();
                }
            }
        }
        return _instance;
    }
}

class LanHan1 {
    private static class SingleLanhan1 {
        public static LanHan1 __instance = new LanHan1();

        static {
            System.out.println("---->类级的内部类被加载");
        }

        public SingleLanhan1() {
        }
    }

    public LanHan1 getInstance() {
        return SingleLanhan1.__instance;
    }
}
