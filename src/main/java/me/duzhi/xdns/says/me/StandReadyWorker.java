package me.duzhi.xdns.says.me;

import java.util.List;

/**
 * @author ashang.peng@aliyun.com
 * @date Dec 19, 2013
 */
public abstract class StandReadyWorker implements StandReady {

	/*
	 * (non-Javadoc)
	 * 
	 * @see me.duzhi.xdns.says.me.StandReady#whatKindOfJobWillYouDo()
	 */
	@Override
	public Class<? extends JobTodo> whatKindOfJobWillYouDo() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see me.duzhi.xdns.says.me.StandReady#setJobs(java.util.List)
	 */
	@Override
	public void setJobs(List<? extends JobTodo> jobs) {

	}

}
