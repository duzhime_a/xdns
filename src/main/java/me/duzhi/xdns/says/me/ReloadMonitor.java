package me.duzhi.xdns.says.me;

import org.apache.commons.logging.Log;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.apache.commons.logging.LogFactory.getLog;

/**
 * @author ashang.peng@aliyun.com
 * @date 2013-12-15
 */
@Component
public class ReloadMonitor implements StandReady {
	private List<ReloadAble> reloadList;
	private ExecutorService reloadExecutors = Executors.newFixedThreadPool(10);
	private static Log logger = getLog(ReloadMonitor.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * me.duzhi.xdns.says.me.StandReady#whatYouShouldDo(java.lang.String)
	 */
	@Override
	public String doWhatYouShouldDo(String whatWifeSays) {
		if (Commands.RELOAD.equalsIgnoreCase(whatWifeSays)) {
			for (final ReloadAble reloadAble : reloadList) {
				reloadExecutors.execute(new Runnable() {

					@Override
					public void run() {
						try {
							reloadAble.reload();
						} catch (Throwable e) {
							logger.warn("oops!My ears!", e);
						}

					}
				});
			}
			return "success";
		}
		return null;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see me.duzhi.xdns.says.me.StandReady#whatWillYouDo()
	 */
	@Override
	public Class<? extends JobTodo> whatKindOfJobWillYouDo() {
		return ReloadAble.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see me.duzhi.xdns.says.me.StandReady#setJobs(java.util.List)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void setJobs(List<? extends JobTodo> jobs) {
		reloadList = (List<ReloadAble>) jobs;
	}

}
