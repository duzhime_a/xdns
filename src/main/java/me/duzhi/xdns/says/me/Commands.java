package me.duzhi.xdns.says.me;

/**
 * @author ashang.peng@aliyun.com
 * @date 2013-12-15
 */
public interface Commands {

	public static final String SHUTDOWN = "shutdown";
	public static final String RELOAD = "reload";
	public static final String CLEAN_CACHE = "clean_cache";

}
