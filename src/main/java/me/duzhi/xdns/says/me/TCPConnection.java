package me.duzhi.xdns.says.me;

import org.apache.commons.logging.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

import static org.apache.commons.logging.LogFactory.getLog;

public class TCPConnection implements Runnable {

	private static Log logger = getLog(TCPConnection.class);

	private Socket socket;

	private TextProcessor textProcessor;

	public TCPConnection(Socket socket, TextProcessor textProcessor) {
		super();
		this.socket = socket;
		this.textProcessor = textProcessor;
	}

	public void run() {

		try {

			try {
				PrintStream out = new PrintStream(socket.getOutputStream());
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(socket.getInputStream()));
				String line = null;
				while ((line = bufferedReader.readLine()) != null) {
					line = line.replaceAll("^\\s+", "").replaceAll("\\s+", "");
					String process = textProcessor.process(line);
					out.println(process);
				}
			} catch (IOException e) {

				logger.debug("Error sending TCP response to "
						+ socket.getRemoteSocketAddress() + ":"
						+ socket.getPort() + ", " + e);

			} finally {
				try {
					socket.close();
				} catch (IOException e) {
				}
			}

		} catch (Throwable e) {

			logger.warn(
					"Error processing TCP connection from "
							+ socket.getRemoteSocketAddress() + ":"
							+ socket.getPort() + ", ", e);
		}
	}
}
