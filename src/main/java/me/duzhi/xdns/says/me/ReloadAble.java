package me.duzhi.xdns.says.me;

/**
 * @author ashang.peng@aliyun.com
 * @date 2013-12-15
 */
public interface ReloadAble extends JobTodo {

	public void reload();

}
