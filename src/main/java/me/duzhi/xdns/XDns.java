package me.duzhi.xdns;

import me.duzhi.xdns.config.Configure;
import me.duzhi.xdns.connector.UDPSocketMonitor;
import me.duzhi.xdns.utils.SpringLocator;
import org.apache.commons.cli.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 * Entry of application. aa
 *
 * @author ashang.peng@aliyun.com
 * @date Dec 14, 2013
 */
@Component
public class XDns {

    private boolean isShutDown = false;

    private static Log logger = LogFactory.getLog(XDns.class);

    private UDPSocketMonitor udpSocketMonitor;

    public void start() throws UnknownHostException, IOException {
        udpSocketMonitor = SpringLocator.getBean(UDPSocketMonitor.class);
        udpSocketMonitor.start();
    }

    private static void parseArgs(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption(new Option("d", true, "home path"));
        CommandLineParser commandLineParser = new PosixParser();
        CommandLine commandLine = commandLineParser.parse(options, args);
        readOptions(commandLine);
    }

    private static void readOptions(CommandLine commandLine) {
        if (commandLine.hasOption("d")) {
            String filename = commandLine.getOptionValue("d");
            Configure.FILE_PATH = filename;
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            parseArgs(args);
        } catch (ParseException e1) {
            logger.warn("parse args error");
        }
        SpringLocator.applicationContext = new ClassPathXmlApplicationContext(
                "classpath*:/spring/applicationContext*.xml");
        XDns xdns = SpringLocator.getBean(XDns.class);
        try {
            xdns.start();
        } catch (UnknownHostException e) {
            logger.warn("init failed ", e);
        } catch (IOException e) {
            logger.warn("init failed ", e);
        }
        while (!xdns.isShutDown) {
            try {
                Thread.sleep(10000000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
