package me.duzhi.xdns.connector;

import me.duzhi.xdns.container.QueryProcesser;
import me.duzhi.xdns.context.RequestContextProcessor;
import me.duzhi.xdns.forward.Forwarder;
import org.apache.commons.logging.Log;
import org.xbill.DNS.Message;

import java.net.DatagramPacket;

import static org.apache.commons.logging.LogFactory.getLog;

public class UDPConnectionWorker implements Runnable {

	private static Log logger = getLog(UDPConnectionWorker.class);

	private final UDPConnectionResponser responser;
	private final DatagramPacket inDataPacket;

	private QueryProcesser queryProcesser;
	private Forwarder forwarder;

	public UDPConnectionWorker(DatagramPacket inDataPacket,
			QueryProcesser queryProcesser, UDPConnectionResponser responser,
			Forwarder forwarder) {
		super();
		this.responser = responser;
		this.inDataPacket = inDataPacket;
		this.queryProcesser = queryProcesser;
		this.forwarder = forwarder;
	}

	public void run() {

		try {

            RequestContextProcessor.processRequest(inDataPacket);
			byte[] response = null;
			response = queryProcesser.process(inDataPacket.getData());
			if (response != null) {
				responser.response(response);
			} else {
				forwarder.forward(inDataPacket.getData(), new Message(
						inDataPacket.getData()), responser);
			}
		} catch (Throwable e) {

			logger.warn(
					"Error processing UDP connection from "
							+ inDataPacket.getSocketAddress() + ", ", e);
		}
	}
}
