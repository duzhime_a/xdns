package me.duzhi.xdns.connector;

import me.duzhi.xdns.concurrent.ThreadPools;
import me.duzhi.xdns.config.Configure;
import me.duzhi.xdns.container.QueryProcesser;
import me.duzhi.xdns.forward.Forwarder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.ExecutorService;

/**
 * Authored by EagleDNS<a href="http://www.unlogic.se/projects/eagledns">
 * http://www.unlogic.se/projects/eagledns</a>
 *
 * @author ashang.peng@aliyun.com
 * @date 2013-12-17
 */
public class UDPSocketMonitor extends Thread {

    private static Log logger = LogFactory.getLog(UDPSocketMonitor.class);

    private InetAddress addr;
    private int port;
    private static final short udpLength = 512;
    private DatagramSocket socket;
    @Autowired
    private QueryProcesser queryProcesser;
    @Autowired
    private Forwarder forwarder;
    @Autowired
    private Configure configure;
    @Autowired
    private ThreadPools threadPools;

    public UDPSocketMonitor(String host, int port) {
        super();
        try {
            this.addr = Inet4Address.getByName(host);
            this.port = port;
            socket = new DatagramSocket(port, addr);
        } catch (IOException e) {
            System.err.println("Startup fail, 53 port is taken or has no privilege");
            logger.error("Startup fail, 53 port is taken or has no privilege", e);
            System.exit(-1);
        }

        this.setDaemon(true);
    }

    @Override
    public void run() {
        ExecutorService executorService = threadPools.getMainProcessExecutor();
        logger.info("Starting UDP socket monitor on address "
                + this.getAddressAndPort());

        while (true) {
            try {

                byte[] in = new byte[udpLength];
                DatagramPacket indp = new DatagramPacket(in, in.length);
                indp.setLength(in.length);
                socket.receive(indp);
                executorService.execute(new UDPConnectionWorker(indp,
                        queryProcesser,
                        new UDPConnectionResponser(socket, indp), forwarder));
            } catch (SocketException e) {

                // This is usally thrown on shutdown
                logger.debug("SocketException thrown from UDP socket on address "
                        + this.getAddressAndPort() + ", " + e);
                break;
            } catch (IOException e) {

                logger.info("IOException thrown by UDP socket on address "
                        + this.getAddressAndPort() + ", " + e);
            }
        }
        logger.info("UDP socket monitor on address " + getAddressAndPort()
                + " shutdown");
    }

    public void closeSocket() throws IOException {

        logger.info("Closing TCP socket monitor on address " + getAddressAndPort()
                + "...");

        this.socket.close();
    }

    public String getAddressAndPort() {

        return addr.getHostAddress() + ":" + port;
    }
}
