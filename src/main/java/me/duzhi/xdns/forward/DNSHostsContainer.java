package me.duzhi.xdns.forward;

import org.apache.commons.logging.Log;
import org.springframework.stereotype.Component;

import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import static org.apache.commons.logging.LogFactory.getLog;

/**
 * @author ashang.peng@aliyun.com
 * @date Dec 21, 2013
 */
@Component
public class DNSHostsContainer {

    private int timeout = 3000;

    private int order;

    private Map<SocketAddress, Integer> requestTimes = new ConcurrentHashMap<SocketAddress, Integer>();

    private static Log logger = getLog(DNSHostsContainer.class);

    public void clearHosts() {
        requestTimes = new ConcurrentHashMap<SocketAddress, Integer>();
        order = 0;
    }

    public void addHost(SocketAddress address) {
        requestTimes.put(address, order++);
        logger.info("add dns address " + address);
    }

    public int getOrder(SocketAddress socketAddress) {
        Integer order = requestTimes.get(socketAddress);
        return order==null?0:order;
    }

    /**
     * @param timeout the timeout to set
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public List<SocketAddress> getAllAvaliableHosts() {
        List<SocketAddress> results = new ArrayList<SocketAddress>();
        Iterator<Entry<SocketAddress, Integer>> iterator = requestTimes
                .entrySet().iterator();
        while (iterator.hasNext()) {
            Entry<SocketAddress, Integer> next = iterator.next();
            results.add(next.getKey());
        }
        return results;
    }

}
