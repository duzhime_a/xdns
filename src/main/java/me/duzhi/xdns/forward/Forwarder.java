package me.duzhi.xdns.forward;

import org.xbill.DNS.Message;
import me.duzhi.xdns.connector.UDPConnectionResponser;

import java.net.SocketAddress;
import java.util.List;

/**
 * @author ashang.peng@aliyun.com
 * @date Jan 16, 2013
 */
public interface Forwarder {

	/**
	 * Forward query bytes to external DNS host(s) and get a valid DNS answer.
	 * 
	 * @param queryBytes
	 * @param query
	 * @return
	 */
	public void forward(final byte[] queryBytes, Message query,
			UDPConnectionResponser responser);

	/**
	 * Forward query bytes to external DNS host(s) and get a valid DNS answer.
	 * 
	 * @param queryBytes
	 * @param query
	 * @return
	 */
	public void forward(final byte[] queryBytes, Message query,
			List<SocketAddress> hosts, UDPConnectionResponser responser);

}
