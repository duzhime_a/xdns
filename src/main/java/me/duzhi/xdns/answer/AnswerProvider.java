package me.duzhi.xdns.answer;

/**
 * Provide the answer.An answerContainer must be registered in
 * {@link AnswerHandler#regitestProviders()} before it takes effect.
 * 
 * @author ashang.peng@aliyun.com
 * @date Dec 14, 2013
 */
public interface AnswerProvider {

	/**
	 * 
	 * @param query
	 * @param type
	 * @return
	 */
	public String getAnswer(String query, int type);

}
