package me.duzhi.xdns.container;

import me.duzhi.xdns.cache.CacheManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xbill.DNS.Message;

import java.io.IOException;

/**
 * Main logic of blackhole.<br/>
 * Process the DNS query and return the answer.
 *
 * @author ashang.peng@aliyun.com
 * @date Dec 14, 2013
 */
@Component
public class QueryProcesser {

    @Autowired
    private HandlerManager handlerManager;

    private static Log logger = LogFactory.getLog(QueryProcesser.class);

    @Autowired
    private CacheManager cacheManager;

    public byte[] process(byte[] queryData) throws IOException {
        Message query = new Message(queryData);
        if (logger.isDebugEnabled()) {
            logger.debug("get query "
                    + query.getQuestion().getName().toString());
        }
        MessageWrapper responseMessage = new MessageWrapper(new Message(query
                .getHeader().getID()));
        for (Handler handler : handlerManager.getHandlers()) {
            boolean handle = handler.handle(new MessageWrapper(query),
                    responseMessage);
            if (!handle) {
                break;
            }
        }
        byte[] response = null;
        if (responseMessage.hasRecord()) {
            response = responseMessage.getMessage().toWire();
            return response;
        }

        byte[] cache = cacheManager.getResponseFromCache(query);
        if (cache != null) {
            return cache;
        } else {
            return null;
        }
    }
}
