package me.duzhi.xdns.config;

import me.duzhi.xdns.answer.AnswerPatternProvider;
import me.duzhi.xdns.answer.CustomAnswerPatternProvider;
import me.duzhi.xdns.says.me.ReloadAble;
import me.duzhi.xdns.utils.DoubleKeyMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * @author ashang.peng@aliyun.com
 * @date Dec 28, 2013
 */
@Component
public class ZonesFileLoader implements InitializingBean, ReloadAble {

    @Autowired
    private Configure configure;

    @Autowired
    private AnswerPatternProvider answerPatternContainer;

    @Autowired
    private CustomAnswerPatternProvider customAnswerPatternProvider;

    private static Log logger = LogFactory.getLog(ZonesFileLoader.class);

    public void readConfig(String filename) {
        try {
            Map<Pattern, String> answerPatternsTemp = new LinkedHashMap<Pattern, String>();
            DoubleKeyMap<String, Pattern, String> customAnswerPatternsTemp = new DoubleKeyMap<String, Pattern, String>(new ConcurrentHashMap<String, Map<Pattern, String>>(), LinkedHashMap.class);
            BufferedReader bufferedReader = new BufferedReader(new FileReader(
                    filename));
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                ZonesPattern zonesPattern = ZonesPattern.parse(line);
                if (zonesPattern==null){
                    continue;
                }
                try {
                    if (zonesPattern.getUserIp() == null) {
                        for (Pattern pattern : zonesPattern.getPatterns()) {
                            answerPatternsTemp.put(pattern, zonesPattern.getTargetIp());
                        }
                    } else {
                        for (Pattern pattern : zonesPattern.getPatterns()) {
                            customAnswerPatternsTemp.put(zonesPattern.getUserIp(), pattern, zonesPattern.getTargetIp());
                        }
                    }
                    logger.info("read config success:\t" + line);
                } catch (Exception e) {
                    logger.warn("parse config line error:\t" + line + "\t" + e);
                }
            }
            answerPatternContainer.setPatterns(answerPatternsTemp);
            customAnswerPatternProvider.setPatterns(customAnswerPatternsTemp);
            bufferedReader.close();
        } catch (Throwable e) {
            logger.warn("read config file failed:" + filename, e);
        }
    }


    /*
     * (non-Javadoc)
     *
     * @see me.duzhi.xdns.says.me.ReloadAble#reload()
     */
    @Override
    public void reload() {
        readConfig(Configure.getZonesFilename());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        reload();
    }

}
