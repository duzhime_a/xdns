#xdns
一个使用Java写的DNS Server , 它的主要特色是可以通过简单配置，将DNS请求导向某些特定IP。同时可以通过特征判断的方式，防止DNS污染。
#用处
XDNS 具有缓存功能，可以一个DNS 缓存服务器使用，以便加速DNS访问。
也支持修改域名配置，配置域名的方式非常简单，与hosts文件一致，并且支持通配符(目前仅支持A记录)。
例如：
127.0.0.1 *.duzhi.me

#开源协议
完全遵循Apache 2.0 License 
#联系方式
EMail：ashang.peng@aliyun.com